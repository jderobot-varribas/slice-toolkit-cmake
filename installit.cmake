#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>

cmake_minimum_required(VERSION 2.6)

install(DIRECTORY cmake/slice2xx/ DESTINATION share/slice2xx/CMake)

install(FILES README.md LICENSE DESTINATION share/slice2xx/)

