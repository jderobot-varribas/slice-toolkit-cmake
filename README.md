# slice-toolkit-cmake

The cmake's toolkit for slice2xx processing.
This toolkit provides solution for complex slice structures,
allowing hiearchies and even multi-layer definitions.

Supported ZeroC Ice's slice generators:
 * slice2cpp
 * slice2python
 * slice2java
