#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>

cmake_minimum_required(VERSION 2.6)

project(slice-toolkit-cmake)

set(CPACK_GENERATOR "DEB")

set(CPACK_DEBIAN_PACKAGE_DEPENDS "cmake(>=2.8.4), ice-translators")
set(CPACK_DEBIAN_PACKAGE_SUGGESTS "ice35-translators")

set(CPACK_PACKAGE_CONTACT "Victor Arribas <v.arribas.urjc@gmail.com>")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "The cmake's toolkit for slice2xx processing.
 This toolkit provides solution for complex slice structures,
 allowing hiearchies and even multi-layer definitions.
 .
 Supported ZeroC Ice's slice generators: slice2cpp,
 slice2python and slice2java.")

set(CPACK_PACKAGE_VERSION_MAJOR 0)
set(CPACK_PACKAGE_VERSION_MINOR 1)
set(CPACK_PACKAGE_VERSION_PATCH 0)

set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE "all")
set(CPACK_DEBIAN_PACKAGE_SECTION "devel")
set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")


include(CPack)
