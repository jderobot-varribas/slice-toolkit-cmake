#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>


LIST(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}")

include(slice2xx-common)
include(slice2cpp)
include(slice2python)
include(slice2java)
