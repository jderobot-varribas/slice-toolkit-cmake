#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>

cmake_minimum_required(VERSION 2.6)


## SLICE_ERROR_LEVEL
# cmake's alert level {STATUS, WARNING, SEND_ERROR}
# Used by every slice's message() >= WARNING
set(SLICE_ERROR_LEVEL WARNING)


## slice_include_directories()
# like include_directories(), but for slice toolchain
macro( slice_include_directories
	INCLUDE_DIR
)
	set(SLICE_ARGS_INCLUDE_DIRECTORIES "${SLICE_ARGS_INCLUDE_DIRECTORIES} -I${INCLUDE_DIR}")
endmacro()


macro( slice_path2fields
	VARIABLE
	slice_rel_path
)
	## https://cmake.org/cmake/help/v3.0/command/get_filename_component.html
	get_filename_component(slice_name ${slice_rel_path} NAME_WE)
	get_filename_component(slice_file_name ${slice_rel_path} NAME)
	get_filename_component(slice_rel_dir   ${slice_rel_path} PATH)
		# PATH for cmake <= 2.8.11
	#get_filename_component(slice_abs_path  ${slice_rel_path} ABSOLUTE)
	#set(slice_abs_path  ${slice_base_path}/${slice_rel_path})
		# get_filename_component(... ABSOLUTE) relies in
		# undocumented base variable to resolve path.
		# Base variable is equal to CMAKE_CURRENT_SOURCE_DIR, but it
		# cannot be hacked to obtain correct absolute path.
		# You can see another face of this bug here:
		# https://cmake.org/pipermail/cmake-developers/2011-July/013661.html
		# Therefore, we need to explicitly prepend prefix to ensure
		# correct path generation. (where slice_base_path is the difference
		# bewteen CMAKE_CURRENT_SOURCE_DIR and target absolute directory.
endmacro()


function( slice_track_files
	AUX_TARGET
		# required cmake target for fulfill this trick
	SLICE_DIRECTORY
		# Absolute path to slice definition tree
)
	file(GLOB_RECURSE ${AUX_TARGET} "${SLICE_DIRECTORY}/**")
	if (${AUX_TARGET})
		add_library(${AUX_TARGET} EXCLUDE_FROM_ALL ${${AUX_TARGET}})
		set_target_properties(${AUX_TARGET} PROPERTIES LINKER_LANGUAGE CXX)
	endif()

endfunction()

## Foot note: macro() popolates current variable scope. function() creates its
# own scope and requires PARENT_SCOPE
