#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>


cmake_minimum_required(VERSION 2.8.4)

function( slice2java
	SLICE_DIRECTORY
		# Absolute path to slice definition tree
	OUTPUT_DIRECTOY
		# Absolute path for generated stuff.
		# expected to be used for inlude_directories()
	#[SLICE_FILES]
		# Optional arg
		# generate definitions only for passed .ice files
		# otherways SLICE_FILES will fetch every .ice file
		# under SLICE_DIRECTORY
)

	if (DEFINED ARGV2)
		set(SLICE_FILES "${ARGV2}")
	else()
		FILE(GLOB_RECURSE SLICE_FILES
			RELATIVE ${SLICE_DIRECTORY}
			"*.ice"
		)
	endif()


	foreach(slice_rel_path ${SLICE_FILES})
		slice_path2fields(slice ${slice_rel_path})
		set(slice_abs_path  ${SLICE_DIRECTORY}/${slice_rel_path})

		set(target_directory ${OUTPUT_DIRECTOY}/${slice_rel_dir})
		file(MAKE_DIRECTORY ${target_directory})

		exec_program(slice2java ${OUTPUT_DIRECTOY}
			ARGS "${slice_abs_path} -I${SLICE_DIRECTORY} ${SLICE_ARGS_INCLUDE_DIRECTORIES}" #--output-dir ${OUTPUT_DIRECTOY}
			OUTPUT_VARIABLE _output
			RETURN_VALUE ret
		)
		if( NOT ret EQUAL 0 )
			message(${SLICE_ERROR_LEVEL} "slice2java: Error procesing ${slice_file_name}:\n ${_output}")
		endif()

	endforeach()

endfunction()
