#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>

cmake_minimum_required(VERSION 2.8.4)

#include(slice2xx-common)


function( slice2cpp
	LIBRARY_NAME
		# name of cmake target (add_library())
	SLICE_DIRECTORY
		# Absolute path to slice definition tree
	OUTPUT_DIRECTOY
		# Absolute path for generated stuff.
		# expected to be used for inlude_directories()
	#[SLICE_FILES]
		# Optional arg
		# generate definitions only for passed .ice files
		# otherways SLICE_FILES will fetch every .ice file
		# under SLICE_DIRECTORY
)

	## Patching CURRENT_XXX_DIR to expect better path-resolve behavior
	# add_library() uses another variable, so we must also patch it.
	set(CMAKE_CURRENT_SOURCE_DIR "${SLICE_DIRECTORY}")
	set(CMAKE_CURRENT_BINARY_DIR "${OUTPUT_DIRECTOY}")
	set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${OUTPUT_DIRECTOY}")

	if (DEFINED ARGV3)
		set(SLICE_FILES "${ARGV3}")
	else()
		FILE(GLOB_RECURSE SLICE_FILES
			RELATIVE ${SLICE_DIRECTORY}
			"*.ice"
		)
	endif()

	foreach(slice_rel_path ${SLICE_FILES})
		slice_path2fields(slice ${slice_rel_path})
		set(slice_abs_path  ${SLICE_DIRECTORY}/${slice_rel_path})

		set(target_directory ${OUTPUT_DIRECTOY}/${slice_rel_dir})
		file(MAKE_DIRECTORY ${target_directory})

		## execute generator
		# exeute_process() is black listed due weird behavior parsing command arguments
		#   doc says: "arguments are passed VERBATIM to the child process". Therefore, space
		#   handling must be done at cmake side
		# exec_program() do it in a simple manner
		#   args is a single string whose spaces will count for command arg-parsing.
		exec_program(slice2cpp ${target_directory}
			ARGS "${slice_abs_path} -I${SLICE_DIRECTORY} ${SLICE_ARGS_INCLUDE_DIRECTORIES} --include-dir ${slice_rel_dir}"
			OUTPUT_VARIABLE _output
			RETURN_VALUE ret
		)
		if( NOT ret EQUAL 0 )
			message(${SLICE_ERROR_LEVEL} "slice2cpp: Error procesing ${slice_file_name}:\n ${_output}")
		endif()

		# fetching .h files relies on include_directories()
		#LIST(APPEND SLICE_CPP_GENERATED ${target_directory}/${slice_name}.h)
		LIST(APPEND SLICE_CPP_GENERATED ${target_directory}/${slice_name}.cpp)
	endforeach()

	include_directories(${OUTPUT_DIRECTOY})
	add_library (${LIBRARY_NAME} SHARED ${SLICE_CPP_GENERATED})
	target_link_libraries(${LIBRARY_NAME} ${ZeroCIce_LIBRARIES})

endfunction()
