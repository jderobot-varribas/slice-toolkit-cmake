#  Copyright (C) 2015-2016 Victor Arribas
#  Copyright (C) 2016 JdeRobot-ng
#  Authors:
#    Victor Arribas <v.arribas.urjc@gmail.com>


### Slice 2 Python
# Required parameters:
# SLICE_DIRECTORY : root directory of slice files
# SLICE_FILES : all ice files to be generated

cmake_minimum_required(VERSION 2.8.4)


function( slice2py
	SLICE_DIRECTORY
		# Absolute path to slice definition tree
	OUTPUT_DIRECTOY
		# Absolute path for generated stuff.
		# expected to be used for inlude_directories()
	#[SLICE_FILES]
		# Optional arg
		# generate definitions only for passed .ice files
		# otherways SLICE_FILES will fetch every .ice file
		# under SLICE_DIRECTORY
)

	if (DEFINED ARGV2)
		set(SLICE_FILES "${ARGV2}")
	else()
		FILE(GLOB_RECURSE SLICE_FILES
			RELATIVE ${SLICE_DIRECTORY}
			"*.ice"
		)
	endif()


	foreach(slice_rel_path ${SLICE_FILES})
		slice_path2fields(slice ${slice_rel_path})
		set(slice_abs_path  ${SLICE_DIRECTORY}/${slice_rel_path})

		### Step 1
		set(target_directory ${OUTPUT_DIRECTOY}/${slice_rel_dir})
		file(MAKE_DIRECTORY ${target_directory})

		exec_program(slice2py ${OUTPUT_DIRECTOY}
			ARGS "${slice_abs_path} -I${SLICE_DIRECTORY} ${SLICE_ARGS_INCLUDE_DIRECTORIES} --output-dir ${target_directory} --all"
			OUTPUT_VARIABLE _output
			RETURN_VALUE ret
		)
		if( NOT ret EQUAL 0 )
			message(${SLICE_ERROR_LEVEL} "slice2py: Error procesing ${slice_file_name}:\n ${_output}")
		endif()

		list(APPEND python_paths ${slice_rel_dir})
	endforeach()

	### Step 2 (pre)
	## full populate of python_paths
	# if hierarchy has empty directories, step 2 can not be bulfilled
	# therefore, we need to add every parent if slice definitions are only placed at tree's leaves
	foreach(python_path ${python_paths})
		get_filename_component(parent_path   ${python_path} PATH)
		if (NOT parent_path STREQUAL SLICE_DIRECTORY)
			list(APPEND python_paths ${parent_path})
			#message(STATUS "slice2py: ${python_path} generates ${parent_path}")
		endif()
	endforeach()


	### Step2: patching __init__.py
	## __init__.py are always placed in a dedicated subtree. This subtree
	# was already done explicit, so we need to revert duplicated directories
	list(REMOVE_DUPLICATES python_paths)
	foreach(python_path ${python_paths})
		if(EXISTS ${OUTPUT_DIRECTOY}/${python_path}/${python_path}/__init__.py)
			file(RENAME ${OUTPUT_DIRECTOY}/${python_path}/${python_path}/__init__.py ${OUTPUT_DIRECTOY}/${python_path}/__init__.py)
			#message(STATUS "slice2py __init__ moved: ${python_path}/${python_path}/__init__.py --> ${python_path}/__init__.py")
		endif()
	endforeach()

	## __init__ do not include submodules, so we need to append it
	foreach(python_path ${python_paths})
		get_filename_component(parent_path   ${python_path} PATH)
		if(EXISTS ${OUTPUT_DIRECTOY}/${parent_path}/__init__.py)
			get_filename_component(module_name   ${python_path} NAME)
			file(APPEND ${OUTPUT_DIRECTOY}/${parent_path}/__init__.py "import ${module_name}\n")
			list(APPEND PATCHED_INITS ${OUTPUT_DIRECTOY}/${parent_path}/__init__.py)
		endif()
	endforeach()

	## put a notice on patched __init__
	foreach(patched_init ${PATCHED_INITS})
		file(APPEND ${patched_init} "\n## __init__ patched by slice2python.cmake\n# (C) Victor Arribas <v.arribas.urjc@gmail.com>\n# Please, be carefull using standard tools to regenerate it.")
	endforeach()

	## Also try to remove unwanted directories. Since we can't list it due are
	# created after, we need to overcome it by use all possible combinations
	foreach(python_path_1 ${python_paths})
		foreach(python_path_2 ${python_paths})
			set(path_to_remove ${OUTPUT_DIRECTOY}/${python_path_1}/${python_path_2})
			if (EXISTS "${path_to_remove}")
				file(REMOVE_RECURSE "${path_to_remove}")
				#message(STATUS "slice2py __init__ removing: ${path_to_remove}")
			endif()
		endforeach()
	endforeach()


endfunction()

